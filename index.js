const GeneralStatService = require('./src/Services/GeneralStatService');
const Util = require('./src/Utils/Util');

global.config= {
    db_write_host: '',
    db_read_host: '',
    db_username: '',
    db_password: '',
    db_port: '',
    db_name: '',
}

exports.handler = async (event) => {
    await Util.getSecrets();

    const service = new GeneralStatService();
    return await service.procesar();
 };
