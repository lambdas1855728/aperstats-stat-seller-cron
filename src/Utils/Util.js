const AWS = require('aws-sdk');

class Util {

     catchError = (name) => {
        switch (name) {
            case "InvalidSignatureException":
                console.error('Invalid Signature Exception');
                break;
            case "ResourceNotFoundException":
                console.error('Resource Not Found Exception');
                break;
            case "FooServiceException":
                console.error('Foo Service Exception');
                break;
            default:
                throw new Error('Code error');
        }
    }

    getSecrets = async () => {
        let config = {}
        global.config.db_name = process.env.db_name
        if (process.env.NODE_ENV == 'local'){
            return new Promise(function(resolve,){
                global.config.db_read_host = process.env.db_read_host;
                global.config.db_write_host = process.env.db_write_host;
                global.config.db_username = process.env.db_username;
                global.config.db_password = process.env.db_password;
                global.config.db_port = process.env.db_port;
                
                resolve(global.config)
            })
        }
        else{
            const secretManager = new AWS.SecretsManager({ region: process.env.REGION });
            return new Promise(function(resolve,reject){
                secretManager.getSecretValue({ SecretId: process.env.secrets }, (err, result) => {
                    if(err){
                        reject(err)
                        console.error(err);
                    }
                    else{
                        const secret = JSON.parse(result.SecretString)
                        global.config.db_read_host = secret.hostro;
                        global.config.db_write_host = secret.host;
                        global.config.db_username = secret.username;
                        global.config.db_password = secret.password;
                        global.config.db_port = secret.port;
                        
                        resolve(config)
                    }
                });
            })
            
        }
    }
}

module.exports = new Util();