const mysql = require('mysql2')
const { POOL_MAX_CONNECTIONS } = require('./Constants');

class Db{
    writePool;
    readPool;
    _instance = false;

    static getInstance = () => {
        if (!this._instance){
            Db._instance = new Db();

            Db._instance.writePool = mysql.createPool({
                connectionLimit: POOL_MAX_CONNECTIONS,
                host: global.config.db_write_host,
                user: global.config.db_username,
                password: global.config.db_password,
                port: global.config.db_port,
                database: global.config.db_name,
            }).promise();
    
            Db._instance.readPool = mysql.createPool({
                connectionLimit: POOL_MAX_CONNECTIONS,
                host: global.config.db_read_host,
                user: global.config.db_username,
                password: global.config.db_password,
                port: global.config.db_port,
                database: global.config.db_name,
            }).promise();
        }
       
        return Db._instance
    }

    query = async function (sql) {
        const conn = await this.readPool.getConnection();
        return conn.query(sql)
            .then(([rows,]) => {
                //console.log('Query results: %o', result);
                return rows;
            })
            .then((r) => {
                conn.release();
                return r;
            });
    };
    
    execute = async function (sql) {
        const conn = await this.writePool.getConnection();
        return conn.query(sql)
            .then(([rows,]) => {
                //console.log('Query results: %o', result);
                return rows;
            })
            .then((r) => {
                conn.release();
                return r;
            });
    };
}


module.exports = Db;