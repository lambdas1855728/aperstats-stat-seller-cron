const Db = require('../Utils/Db')

class StoreRepository {

    db;
    constructor(){
        this.db = Db.getInstance();
    }

    getMensajeSinContestar = async (idSuppliers) => {
        const date_from = new Date();
        date_from.setMonth(date_from.getMonth() - process.env.months_orders_since);
        const date_from_str = date_from.toISOString().slice(0, 10).replace('T', '') + ' 23:59:59';
        const date_to = new Date();
        date_to.setDate(date_to.getDate() - 1);
        const date_to_str = date_to.toISOString().slice(0, 10).replace('T', '') + ' 23:59:59';
        const sql = `SELECT p.id_supplier, COUNT(DISTINCT(if(ct.status = 'open', ct.id_customer_thread, NULL))) AS 'hilos_estado_abierto'
            FROM ps_customer_thread ct
            LEFT JOIN ps_orders o 
            on o.id_order = ct.id_order
            left join ps_order_detail od
            on od.id_order = o.id_order
            left join ps_product p
            on od.product_id = p.id_product
            WHERE o.date_add between "${date_from_str}" and "${date_to_str}" 
            and ct.status = 'open' 
            and p.id_supplier in (${idSuppliers})
            GROUP BY p.id_supplier`;
        console.log(sql)
        return await this.db.query(sql);
    };

    getOrders = async () => {
        const date_from = new Date();
        date_from.setMonth(date_from.getMonth() - process.env.months_orders_since);
        const date_from_str = date_from.toISOString().slice(0, 10).replace('T', '') + ' 23:59:59';
        const date_to = new Date();
        date_to.setDate(date_to.getDate() - 1);
        const date_to_str = date_to.toISOString().slice(0, 10).replace('T', '') + ' 23:59:59';
        const sql = `select s.id_supplier,
                       s.name as seller,
                       p.id_product,
                       o.id_order,
                       o.total_products,
                       o.current_state,
                       o.date_add as orderDateAdd,
                       o.date_upd as orderDateUpd,
                       od.id_order_detail
              from ps_supplier s
              left join ps_product p on p.id_supplier = s.id_supplier
              inner join ps_order_detail od on p.id_product = od.product_id
              left join ps_orders o on od.id_order = o.id_order
              where o.date_add between "${date_from_str}" and "${date_to_str}" 
              order by s.id_supplier;`;
        console.log(sql);
        return await this.db.query(sql);
    };
}

module.exports = StoreRepository;