const AWS = require('aws-sdk');
const { catchError } = require('../Utils/Util');
const _ = require('lodash')

class GeneralStatRepository {
    db;
    generalStatsTable;
    util;

    constructor() {
        if (process.env.NODE_ENV === 'local') {
            this.db = new AWS.DynamoDB.DocumentClient({
                region: process.env.REGION,
                endpoint: process.env.DYNAMODB_ENDPOINT,
                credentials: {
                    accessKeyId: process.env.DYNAMODB_ACCESS_KEY_ID,
                    secretAccessKey: process.env.DYNAMODB_SECRET_ACCESS_KEY
                }
            });
        } else {
            this.db = new AWS.DynamoDB.DocumentClient({
                region: process.env.DYNAMODB_REGION,
            });
        }
        this.generalStatsTable = process.env.table_seller_general_stats;
    }

    guardar = async (stat) => {

        console.log('Do guardar()');
        console.log(stat);
        const params = {
            TableName: this.generalStatsTable,
            Item: stat
        };

        try {
            return await this.db.put(params).promise();
        } catch ({ name }) {
            console.error('sale1')
            catchError(name);
        }
    }

    traerGeneralStats = async () => {
        console.log('Do traerGeneralStats()');
        var params = {
            TableName: this.generalStatsTable,
        };
        try {
            return await this.db.scan(params).promise();
        } catch ({ name }) {
            console.error('sale2')
            catchError(name);
        }
    }

    saveBatch = (arr) => {
        // Map Entries: Maximum number of 25 items.
        const self = this;
        return new Promise(function (resolve, reject) {

            let itemsArray = [];
            for (let i = 0; i < arr.length; i++) {
                const item = {
                    PutRequest: {
                        Item: arr[i]
                    }
                };

                if (item) {
                    itemsArray.push(item);
                }
            }

            for(const chunk of _.chunk(itemsArray,20)){
                const params = {
                    RequestItems: {}
                };
                params['RequestItems'][self.generalStatsTable] = chunk

                self.db.batchWrite(params, function (err, ) {
                    if (err) {
                        console.log(JSON.stringify(chunk))
                        reject(err)
                    }
                    else {
                        console.log('Added ' + chunk.length + ' items to DynamoDB');
                        
                    }
                });
            }

            resolve(true);

        })

    }

}
module.exports = GeneralStatRepository;
