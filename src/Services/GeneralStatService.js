const DynamoRepository = require('../Repository/DynamoRepository');
const StoreRepository = require('../Repository/StoreRepository');
const Order = require('../Entity/Order');
const GeneralStat = require('../Entity/GeneralStat');
const _ = require('lodash');


class GeneralStatService {
    storeRepository;
    dynamoRepository;

    constructor() {
        this.storeRepository = new StoreRepository();
        this.dynamoRepository = new DynamoRepository();
    }

    async procesar() {
        const ordenes = await this.storeRepository.getOrders();
        console.log('Cant Ordenes: ',ordenes.length);
        if (ordenes.length != 0){
            const agrupado = _.groupBy(ordenes, o => o.id_supplier);
            console.log('Suppliers: ',JSON.stringify(Object.keys(agrupado)));
            const mensajeSinContestar = await this.storeRepository.getMensajeSinContestar(Object.keys(agrupado).join(','));
            
            let aux = [];
            for (const val of Object.keys(agrupado)) {
                const a = new GeneralStat(Number(val));
                let arrayOrdenesHechas = [];
                
                agrupado[val].forEach(orden => {
                    const o = new Order(orden);

                    if (o.isApproved) {
                        a.totalVentas += Number(o.total_products);
                        a.totalOrdenes++;
                    }
                    if (o.isUnDelivered) {
                        a.ordenesSinEntregar++;
                    }
                    arrayOrdenesHechas.push(o.id_order);
                })

                const rowCantidadMensajes = _.find(mensajeSinContestar,m=>Number(m.id_supplier) === Number(val)) 
                console.log(rowCantidadMensajes);
                a.mensajesSinContestar = _.isUndefined(rowCantidadMensajes) ? 0 : rowCantidadMensajes.hilos_estado_abierto;

                a.totalVentas = Number(a.totalVentas.toFixed(2));
                a.ticketPromedio = (a.totalVentas !== 0) ? Number((a.totalVentas / a.totalOrdenes).toFixed(2)) : 0;

                aux.push(a);
            }

            await this.dynamoRepository.saveBatch(aux);
            console.log('Cant Stats Saved: ',aux.length);
        }

        return 'OK';
    }
}

module.exports = GeneralStatService;