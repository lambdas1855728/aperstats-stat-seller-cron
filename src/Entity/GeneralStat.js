class GeneralStat{
    id_supplier;
    totalVentas;
    totalOrdenes;
    ticketPromedio;
    ordenesSinEntregar;
    mensajesSinContestar;
    

    constructor(id_supplier){
        this.id_supplier = id_supplier;
        this.totalVentas = 0;
        this.totalOrdenes = 0;
        this.ticketPromedio = 0;
        this.ordenesSinEntregar = 0;
        this.mensajesSinContestar = 0;
    }

    
}


module.exports = GeneralStat;