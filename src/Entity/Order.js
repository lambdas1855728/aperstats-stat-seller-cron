const notApprovedStates = process.env.order_unapproved_states.split(',').map(Number);
const notDeliveredStates = process.env.order_undelivered_states.split(',').map(Number);

class Order {
    id_supplier;
    seller;
    id_product;
    id_order;
    total_products;
    current_state;
    subclassification_id;
    id_customer_thread;
    isApproved = false;
    isUnDelivered = false;
    thread_status;

    constructor(order) {
        this.id_supplier = order.id_supplier;
        this.seller = order.seller;
        this.id_product = order.id_product;
        this.id_order = order.id_order;
        this.total_products = order.total_products;
        this.current_state = order.current_state;
        this.subclassification_id = order.subclassification_id;
        this.id_customer_thread = order.id_customer_thread;
        this.thread_status = order.thread_status
     
        this.isApproved = notApprovedStates.indexOf(Number(this.current_state)) === -1;
        this.isUnDelivered = notDeliveredStates.indexOf(Number(this.current_state)) !== -1;
    }
}

module.exports = Order;